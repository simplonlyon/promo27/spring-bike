package co.simplon.promo27.springbike;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
class SpringbikeApplicationTests {
	@Autowired
	MockMvc mvc;


	@Test
	void testDisplayStations() throws Exception {
		mvc.perform(get("/api/station"))
		.andExpect(status().isOk())
		.andExpect(jsonPath("$[0]['name']").isString())
		.andExpect(jsonPath("$[0]['latitude']").isNumber())
		.andExpect(jsonPath("$[0]['longitude']").isNumber())
		.andExpect(jsonPath("$[0]['slots']").isNumber())
		;

	}



	@Test
	void testDisplaySingleStation() throws Exception {
		mvc.perform(get("/api/station/test-station"))
		.andExpect(status().isOk())
		.andExpect(jsonPath("$['name']").isString())
		.andExpect(jsonPath("$['latitude']").isNumber())
		.andExpect(jsonPath("$['longitude']").isNumber())
		.andExpect(jsonPath("$['slots']").isNumber())
		;

	}


	@Test
	void testDisplaySingleStationNotExists() throws Exception {
		mvc.perform(get("/api/station/blablabal"))
		.andExpect(status().isNotFound())
		;

	}

	@Test
	void testDisplayBikeFromStation() throws Exception {
		mvc.perform(get("/api/station/test-station/bike"))
		.andExpect(status().isOk())
		.andExpect(jsonPath("$[0]['electric']").isBoolean())
		.andExpect(jsonPath("$[0]['id']").isString());
	}

	


	@Test
	void testEmptyStation() throws Exception {
		mvc.perform(get("/api/station/empty-station/bike"))
		.andExpect(status().isOk())
		.andExpect(jsonPath("$.length()").value(0));
	}
	
	@Test
	void testAddStation() throws Exception {
		mvc.perform(post("/api/station")
		.contentType(MediaType.APPLICATION_JSON)
        .content("""
            {
                "name": "Station test",
                "latitude":"0.1",
                "longitude":"0.1",
				"slots":10
            }
        """)).andExpect(status().isCreated())
		.andExpect(jsonPath("$['id']").isString());
	}
	
	@Test
	void testStartRental() throws Exception {
		mvc.perform(post("/api/rent/test-bike"))
		.andExpect(status().isCreated())
		.andExpect(jsonPath("$['bike']['station']").isEmpty())
		.andExpect(jsonPath("$['startDate']").isNotEmpty())
		.andExpect(jsonPath("$['bike']['id']").value("test-bike"))
		.andExpect(jsonPath("$['startStation']['id']").value("test-station"))
		.andExpect(jsonPath("$['endStation']").isEmpty())
		.andExpect(jsonPath("$['endDate']").isEmpty())
		.andExpect(jsonPath("$['id']").isString());
	}

	@Test
	void testStartRentalBikeInUse() throws Exception {
		mvc.perform(post("/api/rent/test-bike-in-use"))
		.andExpect(status().isBadRequest());
	}

	@Test
	void testStartRentalBikeElecEmpty() throws Exception {
		mvc.perform(post("/api/rent/test-bike-elec-empty"))
		.andExpect(status().isBadRequest());
	}

	@Test
	@DirtiesContext //Indique que ce test nécessite de relancer l'application et donc reset la bdd
	void testEndRental() throws Exception {
		mvc.perform(patch("/api/rent/test-rent-in-progress/test-station2"))
		.andExpect(status().isOk())
		.andExpect(jsonPath("$['bike']['station']['id']").value("test-station2"))
		.andExpect(jsonPath("$['startDate']").isNotEmpty())
		.andExpect(jsonPath("$['bike']['id']").value("test-bike-in-use"))
		.andExpect(jsonPath("$['startStation']['id']").value("test-station"))
		.andExpect(jsonPath("$['endStation']['id']").value("test-station2"))
		.andExpect(jsonPath("$['endDate']").isNotEmpty())
		.andExpect(jsonPath("$['cost']").isNumber()); //Avec un getCost dans le Rent qui multiplie par 0.1 le nombre de minutes d'écart entre startDate et endDate au delà de 30
	}

	@Test
	void testEndRentalEndStationFull() throws Exception {
		mvc.perform(patch("/api/rent/test-rent-in-progress/test-station-full"))
		.andExpect(status().isBadRequest());
	}

	@Test
	void testEndRentalAlreadyEnded() throws Exception {
		mvc.perform(patch("/api/rent/test-rent/test-station2"))
		.andExpect(status().isBadRequest());
	}

}
