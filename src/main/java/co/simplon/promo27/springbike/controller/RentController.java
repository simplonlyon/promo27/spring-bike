package co.simplon.promo27.springbike.controller;

import java.time.LocalDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import co.simplon.promo27.springbike.entity.Bike;
import co.simplon.promo27.springbike.entity.Rent;
import co.simplon.promo27.springbike.entity.Station;
import co.simplon.promo27.springbike.repository.BikeRepository;
import co.simplon.promo27.springbike.repository.RentRepository;
import co.simplon.promo27.springbike.repository.StationRepository;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/rent")
public class RentController {

    @Autowired
    private BikeRepository bikeRepo;
    @Autowired
    private RentRepository rentRepo;
    @Autowired
    private StationRepository stationRepo;

    @PostMapping("/{bikeId}")
    @ResponseStatus(HttpStatus.CREATED)
    public Rent startRent(@PathVariable String bikeId) {
        Bike bike = bikeRepo.findById(bikeId)
        .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
        if(bike.getStation() == null) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Bike already in use");
        }
        if(bike.getElectric() && bike.getBattery() < 5){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Bike has empty battery");
        }
        Rent rent = new Rent();
        rent.setStartStation(bike.getStation());
        rent.setBike(bike);
        rent.setStartDate(LocalDateTime.now());
        bike.setStation(null);
        rentRepo.save(rent);
        return rent;
    }

    @PatchMapping("/{rentId}/{stationId}")
    public Rent endRent(@PathVariable String rentId, @PathVariable String stationId) {

        Station station = stationRepo.findById(stationId)
        .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
        if(station.isFull()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Station is full");
        }
        Rent rent = rentRepo.findById(rentId)
        .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
        if(rent.getEndDate() != null) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Rent already ended");
        }

        rent.setEndDate(LocalDateTime.now());
        rent.setEndStation(station);
        rent.getBike().setStation(station);

        rentRepo.save(rent);
        return rent;

    }
}
