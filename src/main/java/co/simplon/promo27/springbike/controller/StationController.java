package co.simplon.promo27.springbike.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import co.simplon.promo27.springbike.entity.Bike;
import co.simplon.promo27.springbike.entity.Station;
import co.simplon.promo27.springbike.repository.BikeRepository;
import co.simplon.promo27.springbike.repository.StationRepository;
import jakarta.validation.Valid;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/station")
public class StationController {
    @Autowired
    private StationRepository stationRepo;

    @Autowired
    private BikeRepository bikeRepo;

    @GetMapping
    public List<Station> getAllStations() {
        return stationRepo.findAll();
    }

    @GetMapping("/{id}")
    public Station one(@PathVariable String id) {
        return stationRepo.findById(id).orElseThrow(()-> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }

    @GetMapping("/{id}/bike")
    public List<Bike> getStationBikes(@PathVariable String id) {
        Station station = stationRepo.findById(id)
        .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));

        return bikeRepo.findByStation(station);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Station create(@RequestBody @Valid Station station) {
        stationRepo.save(station);
        return station;
    }

}
