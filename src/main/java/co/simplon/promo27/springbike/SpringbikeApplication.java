package co.simplon.promo27.springbike;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbikeApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbikeApplication.class, args);
	}

}
