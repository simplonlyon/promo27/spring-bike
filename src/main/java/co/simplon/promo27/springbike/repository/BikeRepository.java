package co.simplon.promo27.springbike.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import co.simplon.promo27.springbike.entity.Bike;
import co.simplon.promo27.springbike.entity.Station;

@Repository
public interface BikeRepository extends JpaRepository<Bike, String>{

    List<Bike> findByStation(Station station);
}
