package co.simplon.promo27.springbike.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import co.simplon.promo27.springbike.entity.Station;

@Repository
public interface StationRepository extends JpaRepository<Station, String> {

}
