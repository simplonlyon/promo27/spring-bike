package co.simplon.promo27.springbike.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import co.simplon.promo27.springbike.entity.Rent;

@Repository
public interface RentRepository extends JpaRepository<Rent,String>{

}
