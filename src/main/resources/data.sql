
INSERT INTO station (id,name,latitude,longitude,slots) VALUES 
("test-station","Pole Pixel", 45.75720588418041, 4.8999061823137495, 10),
("test-station2","Cusset", 45.765373053049565, 4.901005647913872, 5),
("test-station-full","CPAM", 45.76701317566911, 4.893911252948619, 1),
("empty-station","Gare de villeurbanne", 45.75578044545989, 4.891547853283188, 15);

INSERT INTO bike (id,electric,battery,station_id) VALUES
("test-bike", 0, NULL, "test-station"),
("test-bike2", 0, NULL, "test-station2"),
("test-bike-in-use", 0, NULL, NULL),
("test-bike-elec", 1, 75, "test-station"),
("test-bike-elec2", 1, 75, "test-station-full"),
("test-bike-elec-empty", 1, 0, "test-station");


INSERT INTO rent (id,start_date,end_date,bike_id,start_station_id, end_station_id) VALUES
("test-rent", "2024-05-21T10:19:37.778", "2024-05-21T10:49:37.778", "test-bike", "test-station", "test-station2"),
("test-rent2", "2024-05-19T11:20:37.778", "2024-05-19T14:05:37.778", "test-bike2", "test-station2", "test-station"),
("test-rent-in-progress", "2024-05-21T10:19:37.778", NULL, "test-bike-in-use", "test-station", NULL);

